//
//  Numbers.swift
//  LastFrame
//
//  Created by DaffolapMac-43 on 08/11/2019.
//  Copyright © 2019 DaffolapMac-43. All rights reserved.
//

import Foundation

public class Numbers {
    let firstNumber: Int
    let secondNumber: Int

    public init(a: Int, b: Int) {
        self.firstNumber = a
        self.secondNumber = b
    }
}
