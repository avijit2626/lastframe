//NOtes: Demo framework
import Foundation

public class LastFrame: NSObject {

//    var number: Numbers?

    private override init() {
        super.init()
    }

    public class func add(number: Numbers) -> Int {
        return addNumbers(number: number)
    }

    class func addNumbers(number: Numbers) -> Int {
        return number.firstNumber + number.secondNumber
    }
}

extension LastFrame {
    public class func sayHiTo(name: String) {
        print("Hi \(name)")
    }
}
