Pod::Spec.new do |spec|

  spec.name         = "LastFrame"
  spec.version      = "6.0"
  spec.summary      = "A CocoaPods library written in Swift"

  spec.description  = <<-DESC
This CocoaPods library helps you perform calculation.
                   DESC

  spec.homepage     = "https://bitbucket.org/avijit2626/lastframe"
  spec.license      = { :type => "MIT", :text => "The MIT License (MIT) \n Copyright ®" }
  spec.author       = { "avijitgoswami" => "avijitgoswami72@gmail.com" }

  spec.ios.deployment_target = "11.2"
  spec.swift_version = "4.2"

  spec.source        = { :git => "https://avijitsw@bitbucket.org/avijit2626/lastframe.git", :tag => "#{spec.version}" }
  spec.source_files  = "LastFrame/**/*.{h,m,swift}"

end